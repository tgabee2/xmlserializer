﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Serialization;
using XMLSerializerApi.Models;

namespace XMLSerializerApi.Controllers
{
	/// <summary>
	/// Provide the required services for the evaluation project.
	/// </summary>
	public class RequestController : ApiController
    {
		private IRequestRepository Repository;
		private readonly SqlConnection dbConnection = new SqlConnection();

		public RequestController(IRequestRepository repo)
		{
			Repository = repo;
		}

		/// <summary>
		/// Initiate the saving files job.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("api/jobs/saveFiles")]
		public IHttpActionResult SaveFiles()
		{		
			try
			{				
				List<RequestXml> requests = Repository.Requests.ToList();
				XmlSerializerNamespaces nspace = new XmlSerializerNamespaces();
				nspace.Add("", "");
				XmlSerializer serializer = new XmlSerializer(typeof(RequestXml));
				foreach (RequestXml r in requests)
				{
					string fileName = r.Content.Date.ToString("yyyy-MM-dd") + ".xml";
					TextWriter wr = new StreamWriter(AppDomain.CurrentDomain.GetData("DataDirectory").ToString() + @"\" + fileName);
					serializer.Serialize(wr, r, nspace);
					wr.Close();
				}
				return Ok();
			}
			catch
			{
				return InternalServerError();
			}			
		}

		/// <summary>
		/// Receive and save request objects to db.
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		[Route("api/data")]
		public IHttpActionResult AddRequests(IEnumerable<RequestModel> requests)
		{			
			try
			{
				Repository.AddRequests(requests);
			}
			catch
			{
				return InternalServerError();
			}
			return Ok();
		}
    }
}
