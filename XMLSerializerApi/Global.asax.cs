﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using XMLSerializerApi.Models;

namespace XMLSerializerApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

			var builder = new ContainerBuilder();

			var config = GlobalConfiguration.Configuration;
			builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

			builder.RegisterType<SqlRepository>()
				   .As<IRequestRepository>()
				   .InstancePerRequest();

			var container = builder.Build();
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
		}
    }
}
