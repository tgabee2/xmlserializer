﻿using Xunit;
using XMLSerializerApi.Models;
using System;
using NSubstitute;
using System.Collections.Generic;

namespace XMLSerializerApi
{
	public class Tests
	{

		[Fact]
		public void CanChangeName()
		{		
			//Arrange
			RequestModel r = new RequestModel { Index = 1, Name = "John Doe", Date = DateTime.Now, Visits = 2 };

			//Act
			r.Name = "New One";

			//Assert
			Assert.Equal("New One", r.Name);
		}

		[Fact]
		public void NullableTest()
		{
			RequestModel r = new RequestModel { Index = 1, Name = "John Doe", Date = DateTime.Now, Visits = 1 };
			
			Assert.Equal<int?>(1, r.Visits);
		}

		[Fact]
		public void InterfaceTest()
		{
			var reqs = Substitute.For<IEnumerable<RequestXml>>();
			var subs = Substitute.For<IRequestRepository>();

			subs.Requests.Returns(reqs);

			subs.AddRequests(new List<RequestModel>() { new RequestModel() { Index = 1, Name = "John Doe", Visits = null, Date = DateTime.Now } } as IEnumerable<RequestModel> );

			subs.Received(1).AddRequests(Arg.Any<IEnumerable<RequestModel>>());
		}
	}
}