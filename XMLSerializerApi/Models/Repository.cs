﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace XMLSerializerApi.Models
{
	public interface IRequestRepository
	{
		IEnumerable<RequestXml> Requests { get; }
		void AddRequests(IEnumerable<RequestModel> requests);
	}

	public class DummyRepository : IRequestRepository
	{
		public IEnumerable<RequestXml> Requests => new List<RequestXml> {
			new RequestXml { Index = 1, Content = new Content { Name = "John", Visits = 0, Date = DateTime.Now} },
			new RequestXml { Index = 2, Content = new Content { Name = "Valerie", Visits = 23, Date = DateTime.Now.AddYears(-1)} },
			new RequestXml { Index = 3, Content = new Content { Name = "Geroge", Visits = 2, Date = DateTime.Now.AddDays(-45)} },
			new RequestXml { Index = 4, Content = new Content { Name = "Vince", Visits = 6, Date = DateTime.Now.AddDays(-348)} },
			new RequestXml { Index = 5, Content = new Content { Name = "Elizabeth", Visits = 1, Date = DateTime.Now.AddHours(-12)} },
			new RequestXml { Index = 6, Content = new Content { Name = "Ivan", Visits = 0, Date = DateTime.Now.AddMonths(-1)} },
			new RequestXml { Index = 7, Content = new Content { Name = "Peter", Visits = 2, Date = DateTime.Now.AddMonths(-2)} },
			new RequestXml { Index = 8, Content = new Content { Name = "Jim", Visits = 34, Date = DateTime.Now} },
			new RequestXml { Index = 9, Content = new Content { Name = "Venice", Visits = 2, Date = DateTime.Now.AddHours(-200)} }
		};	
		
		public void AddRequests(IEnumerable<RequestModel> requests)
		{
			///Do nothing due to this kind of repo lives in memory and designed for testing.
			return;
		}
	}

	public class SqlRepository : IRequestRepository
	{
		private SqlConnection dbConnection = new SqlConnection();
		private List<RequestXml> requests;

		public SqlRepository()
		{
			requests = new List<RequestXml>();
			//Create a SQL repo.
			//As the briefing doesn't say anything about DAL, use the blody old ADO & SQL commands directly.
			dbConnection.ConnectionString = ConfigurationManager.ConnectionStrings["XMLSerializerConnection"].ToString();
			string commandText = @"SELECT [Id],[Index],[Name],[Visits],[Date] FROM [Requests]";
			SqlDataAdapter adapter = new SqlDataAdapter(commandText, dbConnection);
			DataTable dt = new DataTable();
			adapter.Fill(dt);
			DataTableReader rdr = dt.CreateDataReader();
			while (rdr.Read())
			{
				RequestXml r = new RequestXml()
				{
					Index = (int)rdr["Index"],
					Content = new Content()
					{
						Name = rdr["Name"].ToString(),
						Visits = rdr["Visits"] as int? ?? null,
						Date = (DateTime)rdr["Date"]
					}
				};
				requests.Add(r);
			}
			rdr.Close();
		}

		public void AddRequests(IEnumerable<RequestModel> requests)
		{
			dbConnection.ConnectionString = ConfigurationManager.ConnectionStrings["XMLSerializerConnection"].ToString();
			string commandText = "INSERT INTO Requests ([Index], [Name], [Visits], [Date]) VALUES(@ix, @name, @visits, @date)";
			SqlCommand command = new SqlCommand(commandText, dbConnection);

			foreach (var r in requests)
			{
				command.Parameters.Clear();
				command.Parameters.AddWithValue("@ix", r.Index);
				command.Parameters.AddWithValue("@name", r.Name);
				command.Parameters.AddWithValue("@visits", (object)r.Visits ?? DBNull.Value);
				command.Parameters.AddWithValue("@date", r.Date);

				dbConnection.Open();
				command.ExecuteNonQuery();
				dbConnection.Close();
			}
		}

		public IEnumerable<RequestXml> Requests => requests;

	}
}