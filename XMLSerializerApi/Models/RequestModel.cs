﻿using System;
using System.Linq;
using System.Xml.Serialization;

namespace XMLSerializerApi.Models
{
	/// <summary>
	/// Model for Api.
	/// </summary>
	public class RequestModel
	{
		/// <summary>
		/// Request index.
		/// </summary>
		public int Index { get; set; }
		/// <summary>
		/// Request name.
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Number of visits.
		/// </summary>
		public int? Visits { get; set; }
		/// <summary>
		/// Date of the last visit. Meseems.
		/// </summary>
		public DateTime Date { get; set; }
	}
	   
	/// <summary>
	/// Model for XML serialization.
	/// </summary>
	[Serializable()]
	[XmlRoot("request")]
	public class RequestXml
	{
		/// <summary>
		/// Request id.
		/// </summary>
		[XmlElement("ix")]
		public int Index { get; set; }

		/// <summary>
		/// Content of the request.
		/// </summary>
		[XmlElement("content")]
		public Content Content { get; set; }

	}

	/// <summary>
	/// Content model for the request.
	/// </summary>
	public class Content
	{
		/// <summary>
		/// Request name.
		/// </summary>
		[XmlElement("name")]
		public string Name { get; set; }

		/// <summary>
		/// Serialize nullable property.
		/// </summary>
		/// <returns></returns>
		public bool ShouldSerializeVisits() { return Visits.HasValue; }
		/// <summary>
		/// Number of visits.
		/// </summary>
		[XmlElement("visits")]
		public int? Visits { get; set; }

		/// <summary>
		/// Date of the last visit. Meseems.
		/// </summary>
		[XmlIgnore]
		public DateTime Date { get; set; }

		/// <summary>
		/// Format the Date property.
		/// </summary>
		[XmlElement("dateRequested")]
		public string DateFormat
		{
			get { return this.Date.ToString("yyyy-MM-dd"); }
			set { this.Date = DateTime.Parse(value); }
		}
	}		
}