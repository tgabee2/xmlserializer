﻿using Bogus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using XMLSerializerApi.Models;

namespace SenderConsole
{
	public class Program
	{
		private static readonly HttpClient httpClient = new HttpClient();

		static void Main(string[] args)
		{
			int requestNo;
			try
			{
				switch (args.Length)
				{
					case 0:
						Console.Write("Please specify the number of requests for sending: ");
						string input = Console.ReadLine();
						if (Int32.TryParse(input, out requestNo))
						{
							Run(requestNo).Wait();
						}
						else
						{
							Console.WriteLine("The specified value is not a valid integer, terminating...");
							Console.ReadLine();
						}
						break;
					case 1:
						if (Int32.TryParse(args[0], out requestNo))
						{
							Run(requestNo).Wait();
						}
						else
						{
							Console.WriteLine("The argument is not a valid integer, terminating...");
							Console.ReadLine();
						}
						break;
					default:
						Console.WriteLine("Too many arguments passed!");
						Console.ReadLine();
						break;
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine(String.Format("Oops! An error has occoured: {0}", ex.Message));
			}
		}

		static async Task Run(int numberOfRequests)
		{
			Console.WriteLine(String.Format("Creating {0} Request objects.", numberOfRequests));
			var dummyRequests = new Faker<RequestModel>()
				.RuleFor(r => r.Index, f => f.IndexFaker)
				.RuleFor(r => r.Name, f => f.Person.FullName)
				.RuleForType(typeof(int?), f => (f.Random.Number(1, 10) == 1 ? (int?)null : f.Random.Number(1, 25)))
				.RuleFor(r => r.Date, f => f.Date.Past());
			var requests = dummyRequests.Generate(numberOfRequests);
					
			var json = JsonConvert.SerializeObject(requests);
			Console.WriteLine("Request list is constructed and serialized.");
			Console.WriteLine(String.Format("Content of the request: {0}", json));

			string apiUrl = ConfigurationManager.AppSettings["APIUrl"];
			Console.WriteLine(String.Format("Request is being sent to {0}", apiUrl));
			var response = await Request(apiUrl, json, new Dictionary<string, string>());
			string responseText = await response.Content.ReadAsStringAsync();

			Console.WriteLine(String.Format("Request sent, response: {0}", responseText));
			Console.WriteLine("Job is done, press Enter to terminate.");
			Console.ReadLine();
		}

		static async Task<HttpResponseMessage> Request(string url, string content, Dictionary<string, string> headers)
		{
			var httpRequestMessage = new HttpRequestMessage();
			httpRequestMessage.Method = HttpMethod.Post;
			httpRequestMessage.RequestUri = new Uri(url);
			foreach (var head in headers)
			{
				httpRequestMessage.Headers.Add(head.Key, head.Value);
			}
			HttpContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
			httpRequestMessage.Content = httpContent;

			return await httpClient.SendAsync(httpRequestMessage);
		}
	}
}
