CREATE TABLE [dbo].[Requests] (
    [Id] [int] NOT NULL IDENTITY,
    [Index] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Visits] [int],
	[Date] [datetime] NOT NULL
    CONSTRAINT [PK_dbo.Requests] PRIMARY KEY ([Id])
)